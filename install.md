# Installation

- Copy the installation folder to your server.
- Run `sudo bash ./setup-docker-sh`
- Copy the .env.example form the git repo to the stripes folder
- Edit the .env file
- run the `./setup.sh` from the traefik folder
- Start Traefik, and follow the instructions from the README.md from Traefik
- Start the docker container with `docker-compose up -d`
- Run the `./setup.sh` from the stripes folder
- Start the docker container with `docker-compose up -d`
- Wait about a minute, then visit the URL you set in the .env file in your browser
- Works!