<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class dbBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a backup of the database.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filename = "stripe-backup-" . Carbon::now()->format('Y-m-d');
        $password = md5($filename . env("BACKUP_PASSWORD", "password"));
        $filepath = storage_path() . "/app/backup/stripe-backup-" . Carbon::now()->format('Y-m-d');
//        $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . " >> " . $filepath . ".sql";
        $command = "mysqldump --user=root --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . " >> " . $filepath . ".sql";
        $returnVar = NULL;
        $output  = NULL;

        exec($command, $output, $returnVar);

        $command = "zip -e " . $filepath . ".zip " . $filepath .".sql -P ". $password;

        exec($command, $output, $returnVar);

        $command = "curl -T ".$filepath.".zip ".env("STACK_URL")." --user ".env("STACK_USERNAME").":".env("STACK_PASSWORD")." -k";

        exec($command, $output, $returnVar);

        return 0;

    }
}
