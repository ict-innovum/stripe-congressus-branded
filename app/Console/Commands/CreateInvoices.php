<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
Use \Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class CreateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $stripes = DB::table('stripes')
        ->select('name', 'congressus_user_id', 'user_id' ,DB::raw('COUNT( stripes.id) as \'stripes\''))
        ->leftJoin('users', 'users.id', '=', 'stripes.user_id')
        ->where('stripes.status', '0')
        ->groupBy('users.congressus_user_id','users.name', 'stripes.user_id')
        ->orderBy('users.name','ASC')
        ->get();

        $date = Carbon::now()->format("d-m-Y"); //->toDateTimeString();
        $stripe_offer_id = env("CONGRESSUS_STRIPE_PRODUCT_ID");
        $stripe_workflow_id = env("CONGRESSUS_STRIPE_WORKFLOW_ID");
        $invoiceName = env("INVOICE_BASE_NAME");
        foreach($stripes as $stripe){

            $data = [
                "member_id" => $stripe->congressus_user_id,
                "invoice_reference"=> $invoiceName." - ".$date,
                "invoice_workflow_id"=> $stripe_workflow_id,
                "items" => [
                        [
                            "product_offer_id"=> $stripe_offer_id,
                            "quantity"=> $stripe->stripes,
                        ]
                    ]   
                ];

            $this->info(json_encode($data));
            $response = Http::withToken(env("CONGRESSUS_API_TOKEN"))
                ->withBody(json_encode($data), 'application/json')
                ->post("https://api.congressus.nl/v30/sale-invoices");
            if($response->created() == 1){
                \App\Models\Stripe::where('user_id', $stripe->user_id)->update(['status' => 1]);
            }
        }
    }
}
