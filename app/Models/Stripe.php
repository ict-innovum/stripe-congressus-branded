<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stripe extends Model
{
    use HasFactory;

    const STRIPE_STATUS_UNPAID = 0;
    const STRIPE_STATUS_PAID = 1;

    public function cards()
    {
        return $this->belongsTo('App\Models\rfidtag', 'card');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
