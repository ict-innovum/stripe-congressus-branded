<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


class CateringController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if($user->congressus_access != 1){
            abort(401);
        }

        $uncounted_stripes = \App\Models\Stripe::where('counted', \App\Models\Stripe::STRIPE_STATUS_UNPAID)->count();

        return Inertia::render('Zerocount',
        [
            "uncounted_stripes" => $uncounted_stripes,
            "user" => Auth::user(),
        ]
        );
    }

    public function count(){

        $counted_stripes = \App\Models\Stripe::where('counted', \App\Models\Stripe::STRIPE_STATUS_UNPAID)->count();

        \App\Models\Stripe::where('counted', \App\Models\Stripe::STRIPE_STATUS_UNPAID)->update(['counted' => 1]);

        $uncounted_stripes = \App\Models\Stripe::where('counted', \App\Models\Stripe::STRIPE_STATUS_UNPAID)->count();

        return Inertia::render('Zerocount',
        [
            "uncounted_stripes" => $uncounted_stripes,
            "counted_stripes" => $counted_stripes,
            "user" => Auth::user(),
        ]
        );
    }

}
