<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $stripes = \App\Models\Stripe::where('user_id', $user->id)->orderBy('created_at', 'desc')->take(25)->get();
        $total_stripes = \App\Models\Stripe::where('user_id', $user->id)->count() + $user->previous_lifetime;
        $unpaid_stripes = \App\Models\Stripe::where('user_id', $user->id)->where('status', \App\Models\Stripe::STRIPE_STATUS_UNPAID)->count();

        return Inertia::render('Dashboard',
        [
            "stripes" => $stripes,
            "total_stripes" => $total_stripes,
            "unpaid_stripes" => $unpaid_stripes,
            "user" => Auth::user(),
        ]
        );
        // return view('dashboard', compact("stripes", "total_stripes", "unpaid_stripes"));
    }

}
