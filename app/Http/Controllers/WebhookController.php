<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WebhookController extends Controller
{
    
    public function webhook(Request $request){
        $memberid = $request->input("data.member.id");
        $trigger = $request->input("webhook_event_trigger");
        $memberdata = $request->input("data.member");

        $user = \App\Models\User::all()->where('congressus_user_id', $memberdata['id'])->first();

        // Check for a studentnumber first, if there is none, stop
        $response = Http::withToken(env("CONGRESSUS_API_TOKEN"))
        ->get("https://api.congressus.nl/v30/members/".$memberdata['id']);

        if(!array_key_exists("custom_fields", $response->json())){
            return;
        }

        if(!array_key_exists("field_studentnummer", $response->json()["custom_fields"])){
            return;
        }

        if($response->json()["custom_fields"]["field_studentnummer"]["value"] == ""){
            return;
        }

        $studentnumber = $response->json()["custom_fields"]["field_studentnummer"]["value"];

        if($user == null){

            $user = new \App\Models\User();
            $user->name = $memberdata['first_name'] . " " . $memberdata["last_name"];
            $user->email = $memberdata['email'];
            $user->password = "Nope";
            $user->congressus_user_id = $memberdata['id'];
            $user->congressus_user_name = $memberdata['username'];
            $user->congressus_studentnumber = NULL;
            $user->congressus_is_active = true;
            $user->save();

        }

        if($studentnumber != ""){
            $user->congressus_studentnumber = $studentnumber;
            $user->save();
        }

        if($memberdata["status"]["name"] == "Deregistered"){
           $user->congressus_is_active = false;
           $user->save(); 
        }   

    }

}
