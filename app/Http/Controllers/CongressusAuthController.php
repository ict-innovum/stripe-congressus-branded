<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Auth;
use Cookie;

class CongressusAuthController extends Controller
{


    public function OAuthCallback(Request $request){
        $request = $request->all();

        # Get the access codes
        $response = Http::withBasicAuth(env("CONGRESSUS_OAUTH_CLIENT_ID"), env("CONGRESSUS_OAUTH_CLIENT_SECRET"))
        ->asForm()    
        ->post(env("CONGRESSUS_OAUTH_BASE_URL")."/oauth/token",
            [
                'grant_type' => 'authorization_code',
                'code' => $request['code'],
            ]);

        $json = $response->json();

        // Gather user info
        $response = Http::withToken($json['access_token'])
        ->get(env("CONGRESSUS_OAUTH_BASE_URL")."/oauth/userinfo");

        $jsonUser = $response->json();

        // dd($jsonUser);

        // Check if user exsists
        $user = \App\Models\User::all()->where('congressus_user_id', $jsonUser['user_id'])->first();

        if($user == null){
            // Create a new user
            $user = new \App\Models\User();
            $user->name = $jsonUser['name'];
            $user->email = $jsonUser['email'];
            $user->password = "Nope";
            $user->congressus_user_id = $jsonUser['user_id'];
            $user->congressus_user_name = $jsonUser['username'];
            $user->congressus_studentnumber = $jsonUser['custom']['field_studentnummer'];
            $user->congressus_is_active = $jsonUser['is_active'];
            
            $user->save();
        }

        // dd($jsonUser);

        $access = false;
        if(isset($jsonUser["groups"][env("CONGRESSUS_GROUP_ICT_ID")]) && $access == false){
            $access = $jsonUser["groups"][env("CONGRESSUS_GROUP_ICT_ID")]["active"];
        }
        if(isset($jsonUser["groups"][env("CONGRESSUS_GROUP_BOARD_ID")]) && $access == false){
            $access = $jsonUser["groups"][env("CONGRESSUS_GROUP_BOARD_ID")]["active"];
        }
        if(isset($jsonUser["groups"][env("CONGRESSUS_GROUP_CATERING_ID")]) && $access == false){
            $access = $jsonUser["groups"][env("CONGRESSUS_GROUP_CATERING_ID")]["active"];
        }
        
        if($user->congressus_access != $access){
            $user->congressus_access = $access;
            $user->save();
        }

        // Log the user in.
        Auth::loginUsingId($user->id);

        
        // Redirect to dashboard
        return redirect('/dashboard');
    }

}
