<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use PhpOption\None;
use Illuminate\Support\Str;

class ApiStripeController extends Controller
{
    public function login($key, Request $request ){
        $api = \App\Models\ApiKey::all()->where("apikey", $key)->first();

        if($api == NULL){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        if($api->disabled == 1){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        $studentnumber = $request->input('studentnumber');
        if($studentnumber == "") return response('{"code":401,"msg":"Please input a studentnumber"}', 400)->header('Content-Type', 'application/json');

        $user = NULL;

        $isRfid = str::startsWith($studentnumber, "#");
        if($isRfid) {
            // do the RFID stuff
            // Remove the #
            $studentnumber = substr($studentnumber, 1);

            // Get what RFID tag it is.
            $rfid = \App\Models\RfidCard::where("rfid_code", $studentnumber)->first();

            // If no such card is found
            // if ($rfid == null) return response('{"code":404,"msg":"RFID Card is not found.","code":"' . $studentnumber . '"}', 400)->header('Content-Type', 'application/json');
            if ($rfid == null) return response('{"code":404,"msg":"Your RFID card is not found.","code":"' . $studentnumber . '"}', 400)->header('Content-Type', 'application/json');
            // If the card is is disabled.
            if ($rfid->disabled == 1) return response('{"code":404,"msg":"RFID tag is disabled."}', 400)->header('Content-Type', 'application/json');

            // Get the user;
            $user = $rfid->user;
        } else {
            // Its a student number
            // Get the user corronsponding with the number
            $user = \App\Models\User::where("congressus_studentnumber", $studentnumber)->first();
            // If its not matching
            if ($user == null) {
                if($studentnumber == "404"){
                    return response('{"code":404,"msg":"Girlfriend not found"}', 400)->header('Content-Type', 'application/json');
                }
                // return response('{"code":404,"msg":"No user with this studentnumber has been found"}', 400)->header('Content-Type', 'application/json');
                return response('{"code":404,"msg":"Your studentnumber is not found. You probably need to reregister at stripes.engineeringeindhoven.nl"}', 400)->header('Content-Type', 'application/json');
            }
        }

        if ($user->congressus_is_active == 0)
            return response('{"code":404,"msg":"Your account has been disabled, please contact the board"}', 400)->header('Content-Type', 'application/json');

        $total_stripes = \App\Models\Stripe::where('user_id', $user->id)->count() + $user->previous_lifetime;
        $unpaid_stripes = \App\Models\Stripe::where('user_id', $user->id)->where('status', \App\Models\stripe::STRIPE_STATUS_UNPAID)->count();

        return response('{"code":200,"msg":"You are now logged in!.","name":"' . $user->name . '","unpaid_stripes":"' . $unpaid_stripes . '","total_stripes":"' . $total_stripes . '"}', 200)->header('Content-Type', 'application/json');
    }

    public function stripe($key, Request $request ){
        $api = \App\Models\ApiKey::all()->where("apikey", $key)->first();
        if($api == NULL){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        if($api->disabled == 1){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        $studentnumber = $request->input('studentnumber');
        if($studentnumber == "") return response('{"code":401,"msg":"Please input a studentnumber"}', 400)->header('Content-Type', 'application/json');

        $user = NULL;
        $stripe = new \App\Models\Stripe;

        $isRfid = str::startsWith($studentnumber, "#");
        if($isRfid) {
            // do the RFID stuff
            // Remove the #
            $studentnumber = substr($studentnumber, 1);

            // Get what RFID tag it is.
            $rfid = \App\Models\RfidCard::where("rfid_code", $studentnumber)->first();

            // If no such card is found
            if ($rfid == null) return response('{"code":404,"msg":"RFID Card is not found.","code":"' . $studentnumber . '"}', 400)->header('Content-Type', 'application/json');

            // If the card is is disabled.
            if ($rfid->disabled == 1) return response('{"code":404,"msg":"RFID tag is disabled."}', 400)->header('Content-Type', 'application/json');

            // Get the user;
            $user = $rfid->user;
            $stripe->card = $rfid->name;
        } else {
            // Its a student number
            // Get the user corronsponding with the number
            $user = \App\Models\User::where("congressus_studentnumber", $studentnumber)->first();
            // If its not matching
            if ($user == null) return response('{"code":404,"msg":"No user with this studentnumber has been found"}', 400)->header('Content-Type', 'application/json');
        }

        if ($user->congressus_is_active == 0)
            return response('{"code":404,"msg":"Your account has been disabled, please contact the board"}', 400)->header('Content-Type', 'application/json');

        $stripe->terminal = $api->devicename;
        $stripe->user_id = $user->id;
        $stripe->save();

        return response('{"code":200,"msg":"Thank you for your %%S%% stripe(s)."}', 200)->header('Content-Type', 'application/json');
    }

    public function rfid_register($key, Request $request){
        $api = \App\Models\ApiKey::all()->where("apikey", $key)->first();
        if($api == NULL){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        if($api->disabled == 1){
            return response('{"code":403,"msg":"Not authorized"}', 403)->header('Content-Type', 'application/json');
        }

        $card_id = $request->get('CARD');
        $studentnumber = $request->get('STUDENTNUM');

        if($card_id == null || $studentnumber == null) return response('{"code":404,"msg":"Dude, you are not even trying to register a card (Not all argument are there)"}', 400)->header('Content-Type', 'application/json');

        $card_id = $card_id = substr($card_id,1);

        $tag = \App\Models\RfidCard::all()->where("rfid_code", $card_id)->first();

        if($tag != null) return response('{"code":404,"msg":"This card already belongs to a member! (Or ICT hasn\'t fix the bug yet, and it went well!)."}', 400)->header('Content-Type', 'application/json');

        $user = \App\Models\User::where('congressus_studentnumber',$studentnumber)->first();

        //Check for amount of cards already registered
        $card_amount = \App\Models\RfidCard::where("user_id", $user->id)->count();

        //TODO Change to better amount of cards someone can register!
        if($card_amount < 15) {
            $card = new \App\Models\RfidCard;
            $card->rfid_code = $card_id;
            $card->user_id = $user->id;
            $card->name = "RFID Card";
            $card->save();

            return response('{"code":200}', 200)->header('Content-Type', 'application/json');
        } else {
            return response('{"code":400,"msg":"You already have enough cards, you can delete them in the online envoirement"}', 400)->header('Content-Type', 'application/json');;
        }
    }

    public function teapot(){
        return response('{"code":418,"msg":"I\'m a teapot"}', 418)->header('Content-Type', 'application/json');
    }

}
