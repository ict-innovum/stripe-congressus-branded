<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CardsController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $cards = \App\Models\RfidCard::all()->where("user_id", $user->id);

        return view('cards.index', compact("cards"));
    }

    public function change(Request $request){
        $user = Auth::user();
        $request = $request->all();
        $card = \App\Models\RfidCard::all()->where('id', $request["id"])->first();

        $cardUser = $card->user;

        if($user->id != $cardUser->id){
            return 404;
        }

        $card->name = $request["name"];


        $card->save();

        return redirect()->route('cards.index');
    }

}
