<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CongressusAuthController;
use App\Http\Controllers\CateringController;
use App\Http\Controllers\HomeController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', [HomeController::class, 'index'])->middleware('auth')->name('dashboard');

Route::get('/zerocount', [CateringController::class, 'index'])->middleware('auth')->name('zerocount.index');

Route::post('/zerocount', [CateringController::class, 'count'])->middleware('auth')->name('zerocount.count');

Route::get("/callback/congressus", [CongressusAuthController::class, 'OAuthCallback']);

require __DIR__.'/auth.php';
