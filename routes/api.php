<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebhookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/{api}/login', [App\Http\Controllers\ApiStripeController::class, 'login']);         // Sends the login data back
Route::post('/{api}/stripe', [App\Http\Controllers\ApiStripeController::class, 'stripe']);       // Places a stripe to a user
Route::post('/{api}/rfidRegister', [App\Http\Controllers\ApiStripeController::class, 'rfid_register']);  // Registers a new RFID card to a user

Route::get('/{api}/teapot', [App\Http\Controllers\ApiStripeController::class, 'teapot']);  // Teapot!

Route::post("/webhook/member", [WebhookController::class, 'webhook']);