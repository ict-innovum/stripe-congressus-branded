#!/bin/bash

curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
chmod +x /tmp/get-docker.sh
/tmp/get-docker.sh

sudo apt install docker-compose -y

sudo usermod -aG docker $USER

docker network create myNetwork
