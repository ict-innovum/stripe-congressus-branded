# Striping system for Congresses based assosiations
This project is build by SV. Innovum, on top of the API and OAuth of Congresses

The tool has been build with care, and the purpose of being reused by other assosiations. This version of the code is unbranded and can be customized to fit your needs. 

## Function of the application

The main function is to count the stripes that are placed by the various members using the Touchterminal or other interfaces. The webinterface doesn't include a interface to place stripes.

The system creates invoices at the 26th of the month at 9 in the morning. The invoices need to be manually approved by the treasurer of your assosiation.

For the Catering committee, there is a zerocount tab. Here the committee can see the amount of stripes that were placed since the last time they have done a zerocount. They can press a button to count the stripes, and set the count to zero.

## Running the application

To make it as simple as possible, the application is available as docker container. In the `Installation` folder you will find the scripts to install docker on a fresh debian/ubuntu based server, with the other containers needed to run this application.

Traefik is used as reverse proxy, and to handle the SSL certificates from Let's Encrypt. This is fully automated once started.

MariaDB is used as the backend database. The limited userdata is stored in here.

To start this application, follow the Installation instructions in [install.md](./install.md)

## Contact for own version
If you want your own version of this, but don't have the developers, contact thijs@thijstops.com for information

## Developers
Head developer: Thijs Tops

## Disclaimer
This project is not related to Congresses