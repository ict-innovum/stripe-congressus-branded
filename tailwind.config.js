import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        
        extend: {
            colors: {
                'primary': '#dd8500',
                'innovum': {  
                    DEFAULT: '#DD8500',  
                    50: '#FFD596',  
                    100: '#FFCD81',  
                    200: '#FFBD58',  
                    300: '#FFAC30',  
                    400: '#FF9C07',  
                    500: '#DD8500',  
                    600: '#C47600',  
                    700: '#AA6600',  
                    800: '#915700',  
                    900: '#774800',  
                    950: '#6A4000'
                },
            },
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [forms],
};
